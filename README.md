# minbo - minimal bootstrapper for Node.js
A minimal lazy bootstrapper / dependency resolver for Node.js. Makes it possible to
specify async dependencies without the need to define a resolution order
upfront.

## How to install
```js
npm install minbo
```

## Usage
```js
const minbo = require('minbo');

const config = {
  'const': 1,
  'dynamic': (resolve) => resolve('const').then((val) => {
    return val + 1;
  }),
};
const load = minbo(config);

load('daynamic').then((val) => {
  console.log(val);
});

// => 2
```
