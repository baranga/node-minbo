'use strict';
/**
 * @function configuredLoad
 * @param {(string|string[])} keys
 * @return {Promise.<(*|*[])>}
 */

/**
 * Create configured load function
 * @param {Function} load
 * @param {Array} stack
 * @param {Object} config
 * @param {Map} results
 * @return {configuredLoad}
 */
const configureLoad = (load, stack, config, results) => {
  const configuredLoad = (keys) => load(stack, config, results, keys);
  configuredLoad.isLoaded = (key) => results.has(key);
  configuredLoad.getLoadedKeys = () => Array.from(results.keys());
  return configuredLoad;
};

/**
 * Load dependencies
 * @param {Array} stack
 * @param {Object} config
 * @param {Map} results
 * @param {(string|string[])} keys
 * @return {Promise.<*|*[]>}
 */
const load = (stack, config, results, keys) => Promise.all(
  [].concat(keys).map((key) => {
    // quick solve already loaded keys
    if (results.has(key)) {
      return results.get(key);
    }

    // reject unknown keys
    if (!(key in config)) {
      return Promise.reject(new Error(`Minbo: unkown key: ${key}`));
    }

    // reject cyclic dependencies
    if (stack.indexOf(key) > -1) {
      const dependencyPath = stack.concat(key).join(' -> ');
      return Promise.reject(new Error(`Minbo: cyclic dependency detected: ${dependencyPath}`));
    }

    // determine value
    const keyLoaderOrValue = config[key];
    let result;
    if (typeof keyLoaderOrValue === 'function') {
      // setup next iteration stack and load
      const nextStack = stack.slice();
      nextStack.push(key);
      const nextLoad = configureLoad(load, nextStack, config, results);
      // call loader
      result = keyLoaderOrValue(nextLoad);
    } else {
      // use plain value
      result = keyLoaderOrValue;
    }
    results.set(key, result);

    return result;
  })
).then((results) => Array.isArray(keys) ? results : results[0]);

/**
 * @param {Object.<string, *>} config
 * @return {configuredLoad}
 */
module.exports = (config) => {
  const stack = [];
  const results = new Map();
  return configureLoad(load, stack, config, results);
};
