# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Added a header to the changelog.

### Removed
- Removed gitlab-ci step to publish package versions.

## [0.5.0] - 2022-05-31
### Changed
- Adjusted gitlab ci config to test only current Node.js LTS versions.
- Adjusted minimal required NodeJS version to be 14.
- Updated dev dependencies.

### Fixed
- Corrected usage example in README to use `resolve`.

## [0.4.2] - 2020-08-14
### Fixed
- Adjusted gitlab ci config to test only current Node.js LTS versions.

## [0.4.1] - 2020-08-14
### Fixed
- Updated dev dependencies to fix security issues reported by npm audit.

## [0.4.0] - 2017-12-12
### Added
- Utility methods `isLoaded(key)` and `getLoadedKeys()` are now also exposed to
  internal loader.

## [0.3.0] - 2017-12-07
### Added
- Extended interface with utility methods `isLoaded(key)` and `getLoadedKeys()`
  to query internal state of loader.

## [0.2.0] - 2017-12-07
### Changed
- Switch to promised based approach, no callbacks any more.
- At least node version 4 is required to use the lib.

## [0.1.5] - 2016-12-02
### Fixed
- Adjusted code semantics in tests to be node 4 compatible.

## [0.1.4] - 2016-12-01
### Fixed
- Fixed broken cyclic dependency detection.

## [0.1.3] - 2016-12-01
### Changed
- In case of cyclic dependencies the error message will contain a stack trace
  now.

## [0.1.2] - 2016-10-22
### Fixed
- Nothing. Just a version bump to trigger a gitlab ci run to test publish
  script.

## [0.1.1] - 2016-10-21
### Fixed
- Solve indention issue in gitlab ci configuration.

## 0.1.0 - 2016-10-21
### Added
- Main dependency loading logic.


[Unreleased]: https://gitlab.com/baranga/node-minbo/compare/0.5.0...HEAD
[0.5.0]: https://gitlab.com/baranga/node-minbo/compare/0.4.2...0.5.0
[0.4.2]: https://gitlab.com/baranga/node-minbo/compare/0.4.1...0.4.2
[0.4.1]: https://gitlab.com/baranga/node-minbo/compare/0.4.0...0.4.1
[0.4.0]: https://gitlab.com/baranga/node-minbo/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.com/baranga/node-minbo/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/baranga/node-minbo/compare/0.1.5...0.2.0
[0.1.5]: https://gitlab.com/baranga/node-minbo/compare/0.1.4...0.1.5
[0.1.4]: https://gitlab.com/baranga/node-minbo/compare/0.1.3...0.1.4
[0.1.3]: https://gitlab.com/baranga/node-minbo/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/baranga/node-minbo/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/baranga/node-minbo/compare/0.1.0...0.1.1
