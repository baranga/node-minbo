'use strict';
const assert = require('assert');
const bootstrap = require('../index.js');

describe('Basic', function basic() {
  it('bootstraps simple values', function simple() {
    const load = bootstrap({
      simple: 1
    });
    return load('simple')
      .then((val) => {
        assert.equal(val,  1, 'got simple val');
      });
  });

  it('bootstraps plain values by handler', function handler() {
    const load = bootstrap({
      handler: () => 1
    });
    return load('handler')
      .then((val) => {
        assert.equal(val,  1, 'got val from handler');
      });
  });

  it('bootstraps promisse wrapped values by handler', function handler() {
    const load = bootstrap({
      handler: () => Promise.resolve(1)
    });
    return load('handler')
      .then((val) => {
        assert.equal(val,  1, 'got val from handler');
      });
  });

  it('boostraps multiple values at once', function multi() {
    const load = bootstrap({
      int: 1,
      string: 'test',
      handler: (resolve) => Promise.resolve('handler'),
    });
    return load(['int', 'string', 'handler'])
      .then((values) => {
        assert.deepEqual(values, [1, 'test', 'handler'], 'got values');
      });
  });

  it('handles dependencies', function dependencies() {
    const load = bootstrap({
      dependency: 1,
      depending: (resolve, next) => resolve('dependency').then(val => val + 1),
    });
    return load('depending')
      .then((val) => {
        assert.strictEqual(val, 2, 'got depending value');
      });
  });

  it('returns errors of dependencies', function returnsError() {
    const expectedError = new Error();
    const load = bootstrap({
      error: (resolve) => Promise.reject(expectedError),
    });

    return load('error').catch(err => {
      assert.strictEqual(err, expectedError, 'returns expected error');
    });
  });

  it('reports error on cyclic dependencies', function cyclicDependencies() {
    const load = bootstrap({
      dep1: (resolve) => resolve('dep2'),
      dep2: (resolve) => resolve('dep1'),
    });
    return load('dep1')
      .then(
        () => { throw new Error('got no error'); },
        (err) => {
          // all is ok
          assert.ok(err instanceof Error, 'got proper error')
        }
      );
  });

  it('does not report multiple bootstrapping as cyclic dependency', function doubleIsNotCyclic() {
    const load = bootstrap({
      one: (resolve) => new Promise((resolve, reject) => {
        setTimeout(() => resolve(1), 10);
      }),
    });
    return load(['one', 'one']).then((dependencies) => {
      assert.strictEqual(dependencies[0], 1, 'got val');
      assert.strictEqual(dependencies[1], 1, 'got val');
    });
  });

  it('works on implicit dependencies', function implicit() {
    // d1 -> d3
    // d2 -> d3
    const load = bootstrap({
      d1: (resolve) => resolve('d3'),
      d2: (resolve) => resolve('d3'),
      d3: (resolve) => new Promise((resolve, reject) => {
        setTimeout(() => resolve('d'), 10);
      }),
    });
    return load(['d1', 'd2']).then((dependencies) => {
      assert.strictEqual(dependencies[0], 'd', 'got val');
      assert.strictEqual(dependencies[1], 'd', 'got val');
    });
  });

  it('reports errors for undefined keys', function undefinedErrors() {
    const load = bootstrap({});
    return load('undefined').then(
      () => { throw new Error('got no error') },
      (err) => {
        assert.ok(err instanceof Error, 'returned error');
      }
    );
  });

  it('externally exposes loading status of single keys', function externalExposeLoadingStatus() {
    const load = bootstrap({dep1: 1, dep2: 2});
    return load('dep1').then(() => {
      const loadedKeyIsLoaded = load.isLoaded('dep1');
      const notLoadedKeyIsLoaded = load.isLoaded('dep2');

      assert.strictEqual(loadedKeyIsLoaded, true, 'loaded key is reported as loaded');
      assert.strictEqual(notLoadedKeyIsLoaded, false, 'not loaded key is reported as loaded');
    });
  });

  it('interally exposes loading status of single keys', function internalExposeLoadingStatus() {
    let dep1Loaded = void 0;
    let dep2Loaded = void 0;
    const load = bootstrap({
      dep1: 1,
      dep2: 2,
      check: (internalLoad) => internalLoad('dep1').then(() => {
        dep1Loaded = internalLoad.isLoaded('dep1');
        dep2Loaded = internalLoad.isLoaded('dep2');
      }),
    });

    return load('check').then(() => {
      assert.strictEqual(dep1Loaded, true, 'loaded key is reported as loaded');
      assert.strictEqual(dep2Loaded, false, 'not loaded key is reported as loaded');
    });
  });

  it('externally exposes all loaded keys', function externalExposeLoadingStatus() {
    const load = bootstrap({dep1: 1, dep2: 2});
    return load('dep1').then(() => {
      const loadedKeys = load.getLoadedKeys();

      assert.deepStrictEqual(loadedKeys, ['dep1'], 'loaded keys are reported as loaded');
    });
  });

  it('interally exposes all loaded keys', function internalExposeLoadingStatus() {
    let loadedKeysOnDeferredCall = void 0;
    let loadedKeysOnDirectCall = void 0;
    const load = bootstrap({
      dep1: 1,
      dep2: 2,
      check: (internalLoad) => {
        loadedKeysOnDirectCall = internalLoad.getLoadedKeys();
        return internalLoad('dep1').then(() => {
          loadedKeysOnDeferredCall = internalLoad.getLoadedKeys();
        });
      },
    });

    return load('check').then(() => {
      assert.deepStrictEqual(loadedKeysOnDirectCall, [], 'loaded keys are reported as loaded');
      assert.ok(Array.isArray(loadedKeysOnDeferredCall), 'getLoadedKeys() returns a array in deferred calls');
      assert.deepStrictEqual(loadedKeysOnDeferredCall.sort(), ['check', 'dep1'], 'loaded keys are reported as loaded');
    });
  });
});
